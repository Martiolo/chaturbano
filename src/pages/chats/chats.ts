import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { ChatDetailsPage } from '../chat-details/chat-details';

/**
 * Generated class for the ChatsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-chats',
  templateUrl: 'chats.html',
})
export class ChatsPage {

  names: string[];
  notes: string[];
  items: Array<{title: string, note: string}>;

  constructor(public navCtrl: NavController, public navParams: NavParams) {

    // Let's populate this page with some filler content for funzies
    this.names = ['Mario', 'Pala', 'Vane', 'Sergio', 'Sofi', 'Crispi',
    'Diego', 'Nestor', 'Mama Largo'];

    this.notes = ['Quiero perico', 'Yo quiero color', 'Yo quiero es trago', 'Yo mas perico porfa', 'Yo no quiero un culo', 'Yo quiero unos 3 pericos',
    'Hola que tanto tienen', 'Estoy perdiendo el tiempo', 'Hola quiero H'];

    this.initializeItems();

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ChatsPage');
  }

  getItems(ev) {
    // Reset items back to all of the items
    this.initializeItems();

    // set val to the value of the ev target
    var val = ev.target.value;

    // if the value is an empty string don't filter the items
    if (val && val.trim() != '') {
      this.items = this.items.filter((item) => {
        return (item.title.toLowerCase().indexOf(val.toLowerCase()) > -1);
      })
    }
  }

  initializeItems() {
    this.items = [];
    for (let i = 1; i < this.names.length; i++) {
      this.items.push({
        title: this.names[i],
        note: this.notes[i]
      });
    }
  }

  itemTapped(event, item) {
    // That's right, we're pushing to ourselves!
    this.navCtrl.push(ChatDetailsPage, {
      item: item
    });
  }

}

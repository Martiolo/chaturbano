import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';
import { ChatsPage } from '../chats/chats';
import { RegisterPage } from '../register/register';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  constructor(public navCtrl: NavController) {

  }

  login() {
    this.navCtrl.setRoot(ChatsPage);
  }

  register() {
    this.navCtrl.setRoot(RegisterPage);
  }

}
